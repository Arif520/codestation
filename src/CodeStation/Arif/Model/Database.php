<?php
/**
 * Created by PhpStorm.
 * User: Code Station Student
 * Date: 7/3/2018
 * Time: 5:29 PM
 */

namespace App\Model;

use PDO,PDOException;

class Database
{
    public $DBH;

    public function __construct()
    {
        try{
            $this->DBH = new PDO('mysql:host=localhost;dbname=cs_jpl_b2_atomic_project', "root", "");
            echo "Database Connection Successfuly <br>";
            //$this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        }
        catch(PDOException $error){

            echo "Database Error: ". $error->getMessage();
        }
    }


}